package AvactisPO;
import AvactisPackage.Screenshot;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;





public class AvactisRegisterPO {
	WebDriver driver;
	WebDriverWait wait;
	Actions builder;
	Screenshot st;
	//Comments
	
	public AvactisRegisterPO() {
	System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
	driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("http://localhost/avactis");
	wait = new WebDriverWait(driver, 30);	
	builder=new Actions(driver);
	st=new Screenshot();
	}
	private enum Element
	{ 
		MY_ACCOUNT(By.xpath("//a[text()='My Account']")),
		REGISTER_BUTTON(By.xpath("//button[text()='Register']")),
		EMAIL_FIELD(By.xpath("//input[@name='customer_info[Customer][Email]']")),
		PASSWORD_FIELD(By.xpath("//input[@name='customer_info[Customer][Password]']")),
		REPASSWORD_FIELD(By.xpath("//input[@name='customer_info[Customer][RePassword]']")),
		FIRSTNAME_FIELD(By.xpath("//input[@name='customer_info[Customer][FirstName]']")),
		LASTNAME_FIELD(By.xpath("//input[@name='customer_info[Customer][LastName]']")),
		COUNTRY_FIELD(By.xpath("//select[@name='customer_info[Customer][Country]']")),
		STATE_FIELD(By.xpath("//select[@id='customer_info_Customer_State']")),
		ZIPCODE_FIELD(By.xpath("//input[@name='customer_info[Customer][ZipCode]']")),
		CITY_FIELD(By.xpath("//input[@name='customer_info[Customer][City]']")),
		ADDLINE_1(By.xpath("//input[@name='customer_info[Customer][Streetline1]']")),
		ADDLINE_2(By.xpath("//input[@name='customer_info[Customer][Streetline2]']")),
		CONTACT_NO(By.xpath("//input[@name='customer_info[Customer][Phone]']")),
		REGISTER_OKAY(By.xpath("//input[@value='Register']")),
		HOMEPAGE_WELCOME(By.xpath("//li//span[text()='Welcome,']")),
		ADMINEMAIL_FIELD(By.xpath("(//input[@name='AdminEmail'])[1]")),
		ADMINPASSWORD_FIELD(By.xpath("(//input[@name='Password'])[1]")),
		SIGNIN_BUTTON(By.xpath("//button[text()='Sign In ']")),
		CUSTOMER_BUTTON(By.xpath("(//span[text()='Customers']/parent::a)[1]")),
		DELETE_BUTTON(By.xpath("(//div[@id='DeleteButton2'])[1]")),
		OKAY_BUTTON(By.xpath("//button[text()='OK']"))
		;By findby;
		private Element(By locator) 
		{
			this.findby=locator;
		}
	}
	
	public boolean openRegisterPage() throws IOException
	{
		boolean state=false;
		builder.moveToElement(ret(Element.MY_ACCOUNT.findby)).click().perform();
		builder.moveToElement(ret(Element.REGISTER_BUTTON.findby)).click().perform();
		String expectedpage = "http://localhost/avactis/register.php";
		String actualPage = driver.getCurrentUrl();
		if(expectedpage.equals(actualPage))
		state=true;	
		st.TakeScreenshot(driver);
		return state;		
	}
	public boolean fillDetails(String email, String password, String repasswd, String FN, String LN, String Cntry,
			String Ste, String ZIP, String City, String Add1, String Add2, String phone) throws IOException
	{
		boolean state=false;
		ret(Element.EMAIL_FIELD.findby).sendKeys(email);
		ret(Element.PASSWORD_FIELD.findby).sendKeys(password);
		ret(Element.REPASSWORD_FIELD.findby).sendKeys(repasswd);
		ret(Element.FIRSTNAME_FIELD.findby).sendKeys(FN);
		ret(Element.LASTNAME_FIELD.findby).sendKeys(LN);
		ret(Element.COUNTRY_FIELD.findby).click();
		ret(By.xpath("//select[@name='customer_info[Customer][Country]']/option[text()='"+Cntry+"']")).click();
		ret(Element.STATE_FIELD.findby).click();
		ret(By.xpath("//select[@id='customer_info_Customer_State']/option[text()='"+Ste+"']")).click();
		ret(Element.ZIPCODE_FIELD.findby).sendKeys(ZIP);
		ret(Element.CITY_FIELD.findby).sendKeys(City);
		ret(Element.ADDLINE_1.findby).sendKeys(Add1);
		ret(Element.ADDLINE_2.findby).sendKeys(Add2);
		ret(Element.CONTACT_NO.findby).sendKeys(phone);
		ret(Element.REGISTER_OKAY.findby).click();
		String expectedpage1 = "http://localhost/avactis/home.php";
		String actualPage1 = driver.getCurrentUrl();
		if(expectedpage1.equals(actualPage1))
		state=true;
		st.TakeScreenshot(driver);
		return state;
	}
	public boolean homePage()
	{    boolean state=false;
		state=ret(Element.HOMEPAGE_WELCOME.findby).isDisplayed();
		driver.quit();
		return state;
	}
	
	public boolean clearRegUser(String Admin,String pwd,String emailid)
	{
		boolean state=false;
		System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://localhost/avactis/avactis-system/admin/signin.php");
		ret(Element.ADMINEMAIL_FIELD.findby).sendKeys(Admin);
		ret(Element.ADMINPASSWORD_FIELD.findby).sendKeys(pwd);
		ret(Element.SIGNIN_BUTTON.findby).click();
		ret(Element.CUSTOMER_BUTTON.findby).click();
		ret(By.xpath("//td/a[contains(text(),'" + emailid + "')]/parent::td/preceding-sibling::td[2]")).click();
		ret(Element.DELETE_BUTTON.findby).click();
		ret(Element.OKAY_BUTTON.findby).click();
		state=true;
		driver.quit();
		return state;
	}
	
	
	public WebElement ret(By loc)
	{
		WebElement element = wait.until(
				ExpectedConditions.elementToBeClickable(driver.findElement(loc)));
				return element;
	}
	
	
	
}
