package AvactisPO;

import static org.testng.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import AvactisPackage.Screenshot;

public class AvactisShoppingPO {
	WebDriver driver;
	WebDriverWait wait;
	Actions builder;
	HashMap<String, String> prod;
	static int itemnum = 2;
	ConfigData data;
	public AvactisShoppingPO() {
		data=new ConfigData();
		System.setProperty("webdriver.chrome.driver",data.getDriverPath());
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(data.getApplicationHostUrl());
		wait = new WebDriverWait(driver, data.getImplicitlyWait());
		builder=new Actions(driver);
		prod = new HashMap<String, String>();
		}
	
	private enum Element
	{ 
		SIGNIN_BUTTON(By.xpath("//li//a[text()='Sign In']")),
		REMEMBERME_CHECKBOX(By.xpath("//input[@name='remember_me']")),
		EMAIL_FIELD(By.xpath("//input[@name='email']")),
		PASSWORD_FIELD(By.xpath("//input[@name='passwd']")),
		LOGIN_BUTTON(By.xpath("//input[@value='Sign In']")),
		SITELOGO(By.xpath("//a[@class='site-logo']")),
		CART(By.xpath("//i[@class='fa fa-shopping-cart']")),
		CHECKOUT(By.xpath("//div[@class='text-right']/a[text()='Checkout']")),
		ADDCONFIRM_CHECKBOX(By.xpath("//input[@name='checkbox_shipping_same_as_billing']")),
		CHECKOUT_AGAIN(By.xpath("(//div[@class='checkout_buttons']/input[@value='Continue Checkout'])[1]")),
		SHIPPING_MODE(By.xpath("//div[@class='shipping_method_name']/label[contains(text(),'National Priority Airmail')]/input")),
		SHIPPINGMODE_NAME(By.xpath("//div[@class='shipping_method_name']/label[contains(text(),'National Priority Airmail')]")),
		SHIPPINGMODE_COST(By.xpath("//div[@class='shipping_method_name']/label[contains(text(),'National Priority Airmail')]/parent::div/following-sibling::div")),
		CONTINUE_CHECKOUT(By.xpath("(//div[@class='checkout_buttons']/input[@value='Continue Checkout'])[2]")),
		PLACE_ORDER(By.xpath("//div[@class='checkout_buttons']/input[@value='Place Order']")),
		CONTINUE_SHOPPING(By.xpath("//div[@class='checkout_buttons']/a[text()='Continue Shopping']")),
		ADMINEMAIL(By.xpath("(//input[@name='AdminEmail'])[1]")),
		ADMINPASSWORD(By.xpath("(//input[@name='Password'])[1]")),
		ADMINSIGNIN_BUTTON(By.xpath("//button[text()='Sign In ']")),
		ADMINORDERS(By.xpath("(//span[text()='Orders']/parent::a)[1]")),
		;By findby;
		private Element(By locator) 
		{
			this.findby=locator;
		}
	}
	
	public void login(String Uname, String Pwd)
	{
		builder.moveToElement(ret(Element.SIGNIN_BUTTON.findby)).click().perform();
		builder.moveToElement(ret(Element.EMAIL_FIELD.findby)).click().sendKeys(Uname).perform();
		builder.moveToElement(ret(Element.PASSWORD_FIELD.findby)).click().sendKeys(Pwd).perform();
		builder.moveToElement(ret(Element.REMEMBERME_CHECKBOX.findby)).click().perform();
		builder.moveToElement(ret(Element.LOGIN_BUTTON.findby)).click().perform();
	}
	
	public boolean addProduct(String Sno,String Category, String Product, String item)
	{ boolean state=false;
	builder.moveToElement(ret(Element.SITELOGO.findby)).click().perform();	
	builder.moveToElement(ret((By.xpath("//div[@class='header-navigation']/ul/li/a[text()='" + Category + "']")))).perform();	
	builder.moveToElement(ret(By.xpath("//div[@class='header-navigation']/ul/li/a[text()='"+ Category + "']/parent::li/ul/li/a[text()='" + Product + "']"))).click().perform();
	builder.moveToElement(ret((By.xpath("//div[@class='product_name']/h3[text()='" + item
						+ "']/ancestor::a/following-sibling::div[@class='product_buttons']/input[@value='Add To Cart']")))).click().perform();
	builder.moveToElement(ret(Element.CART.findby)).perform();
	String cartItem = ret(By.xpath("//div[@class='top-cart-content-wrapper  minicart']//li[" + Sno + "]/strong/a")).getText();
	String cartItemPrice =ret(By.xpath("//div[@class='top-cart-content-wrapper  minicart']//li[" + Sno + "]/em")).getText();
	assertEquals(item, cartItem, "Item is not added to cart");
	state=true;
	prod.put(cartItem, cartItemPrice);
	return state;
	}
	
	public boolean checkout() throws InterruptedException
	{   boolean state=false;
	    builder.moveToElement(ret(Element.CHECKOUT.findby)).click().perform();
	    builder.moveToElement(ret(Element.ADDCONFIRM_CHECKBOX.findby)).click().perform();
	    builder.moveToElement(ret(Element.CHECKOUT_AGAIN.findby)).click().perform();
	    Thread.sleep(2000);
	    builder.moveToElement(ret(Element.SHIPPING_MODE.findby)).click().perform();
		String Shippingmodename =ret(Element.SHIPPINGMODE_NAME.findby).getText();
		String Shippingmodecost =ret(Element.SHIPPINGMODE_COST.findby).getText();
		prod.put(Shippingmodename, Shippingmodecost);
		builder.moveToElement(ret(Element.CONTINUE_CHECKOUT.findby)).click().perform();
		Thread.sleep(5000);
		for (Entry<String, String> data : prod.entrySet()) {
		if (itemnum <= 4) {
		if (data.getKey().equals(ret(By.xpath("//table[@class='order_items without_images']/tbody/tr["+itemnum+"]/td[@class='product_data']/a")).getText()))
		{
	    Assert.assertEquals(
							ret(By.xpath("//table[@class='order_items without_images']/tbody/tr["+itemnum+"]/td[@class='product_sale_price']/span")).getText(),
							data.getValue());
				   }
				itemnum++;
			   }
		   }
		builder.moveToElement(ret(Element.PLACE_ORDER.findby)).click().perform();
		builder.moveToElement(ret(Element.CONTINUE_SHOPPING.findby)).click().perform();
		state=true;
		driver.quit();
		return state;
	    }
	public boolean checkOrders(String uname,String pwd)
	{boolean state=false;
	driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get(data.getApplicationAdminUrl());
	ret(Element.ADMINEMAIL.findby).sendKeys(uname);
	ret(Element.ADMINPASSWORD.findby).sendKeys(pwd);
	ret(Element.ADMINSIGNIN_BUTTON.findby).click();
	builder.moveToElement(ret(Element.ADMINORDERS.findby)).click().perform();
	state=true;
	return state;
	}
	
	public WebElement ret(By loc)
	{
		WebElement element = wait.until(
				ExpectedConditions.elementToBeClickable(driver.findElement(loc)));
				return element;
	}
}
