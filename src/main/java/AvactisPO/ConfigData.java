package AvactisPO;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigData {
	private Properties properties;
	 private final String propertyFilePath= "src\\test\\resources\\PropertiesFile\\Avactis.properties";
	 
	 
	 public ConfigData(){
	 BufferedReader reader;
	 try {
	 reader = new BufferedReader(new FileReader(propertyFilePath));
	 properties = new Properties();
	 try {
	 properties.load(reader);
	 reader.close();
	 } catch (IOException e) {
	 e.printStackTrace();
	 }
	 } catch (FileNotFoundException e) {
	 e.printStackTrace();
	 throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
	 } 
	 }
	 
	 public String getDriverPath(){
	 String driverpath = properties.getProperty("driverpath");
	 if(driverpath!= null) return driverpath;
	 else throw new RuntimeException("driverPath not specified in the Configuration.properties file."); 
	 }
	 
	 public long getImplicitlyWait() { 
	 String Wait = properties.getProperty("wait");
	 if(Wait != null) return Long.parseLong(Wait);
	 else throw new RuntimeException("implicitlyWait not specified in the Configuration.properties file."); 
	 }
	 
	 public String getApplicationHostUrl() {
	 String hosturl = properties.getProperty("hosturl");
	 if(hosturl != null) return hosturl;
	 else throw new RuntimeException("url not specified in the Configuration.properties file.");
	 }
	 public String getAdminuname() {
	 String Adminuname = properties.getProperty("Adminuname");
	 if(Adminuname != null) return Adminuname;
	 else throw new RuntimeException("Adminname not specified in the Configuration.properties file.");
	 }
	 public String getApplicationAdminUrl() {
	 String Adminurl = properties.getProperty("Adminurl");
	 if(Adminurl != null) return Adminurl;
	 else throw new RuntimeException("Adminurl not specified in the Configuration.properties file.");
	 }
	 public String getAdminpwd() {
	 String Adminpwd = properties.getProperty("Adminpwd");
	 if(Adminpwd != null) return Adminpwd;
	 else throw new RuntimeException("Adminpwd not specified in the Configuration.properties file.");
	 }
}
