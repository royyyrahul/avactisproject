package AvactisPO;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.testng.annotations.DataProvider;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

public class DataProv {
	@DataProvider(name = "RegisterAvactis")
	public Object[][] dataProviderMethod() {
		Object[][] retObjArr = getDataFromXLSUsingJXL("src\\test\\resources\\Avactis.xls", "Register", "Pointer");
		return (retObjArr);
	}

	public String[][] getDataFromXLSUsingJXL(String xlFilePath, String sheetName, String tableName) {
		String[][] tabArray = null;
		try {
			Workbook workbook = Workbook.getWorkbook(new File(xlFilePath));
			Sheet sheet = workbook.getSheet(sheetName);
			Cell tableStart = sheet.findCell(tableName);
			int startRow, startCol, endRow, endCol, ci, cj;
			startRow = tableStart.getRow();
			startCol = tableStart.getColumn();
			Cell tableEnd = sheet.findCell(tableName, startCol + 1, startRow + 1, 100, 64000, false);
			endRow = tableEnd.getRow();
			endCol = tableEnd.getColumn();
			System.out.println("startRow=" + startRow + ", endRow=" + endRow + ", " + "startCol=" + startCol
					+ ", endCol=" + endCol);
			tabArray = new String[endRow - startRow - 1][endCol - startCol - 1];
			ci = 0; // array row
			// ci=0,i=3, j=3,cj=1
			for (int i = startRow + 1; i < endRow; i++, ci++) {// i represents xls row
				cj = 0;// array column
				for (int j = startCol + 1; j < endCol; j++, cj++) {// j represents xls column
					tabArray[ci][cj] = sheet.getCell(j, i).getContents();
				}
			}
		} catch (Exception e) {
			System.out.println("Please check if file path, sheet name and tag name are correct");

		}

		return (tabArray);
	}
	@DataProvider(name = "ProductsAvactis")
	public Object[][] dataProviderMethodPoi() {
		Object[][] retObjArr = getDataFromXLSUsingPOI("src\\test\\resources\\Avactis.xls", "Products");
		return (retObjArr);
	}

	public String[][] getDataFromXLSUsingPOI(String xlFilePath, String sheetName) {
		String[][] tabArray = null;
		try {
			FileInputStream file = new FileInputStream(new File(xlFilePath));
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			HSSFSheet sheet = workbook.getSheet(sheetName);
			int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
			int colCount = sheet.getRow(0).getLastCellNum();
			tabArray = new String[rowCount][colCount];
			System.out.println("r" + rowCount + "c" + colCount + tabArray.length);
			for (int i = 1; i <= rowCount; i++) {

				Row row = sheet.getRow(i);

				for (int j = 0; j < row.getLastCellNum(); j++) {

					tabArray[i - 1][j] = row.getCell(j).getStringCellValue();

				}
			}
		}

		catch (Exception e) {
			System.out.println(e);

		}

		return (tabArray);
	}

}
