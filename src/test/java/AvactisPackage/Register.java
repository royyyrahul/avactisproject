package AvactisPackage;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import AvactisPO.AvactisRegisterPO;
import AvactisPO.ConfigData;
import AvactisPO.DataProv;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

public class Register {
	WebDriver driver;
	static String emailid = "";
	boolean registerstate = false;
	WebDriverWait wait;
    AvactisRegisterPO regobj;
    ConfigData data=new ConfigData();
    //Logger log = Logger.getLogger("Shoppinglogs");
    Logger log;
	@Test(dataProvider = "RegisterAvactis",dataProviderClass=DataProv.class)
	public void AvactisRegisterTest(String email, String password, String repasswd, String FN, String LN, String Cntry,
			String Ste, String ZIP, String City, String Add1, String Add2, String phone) throws InterruptedException, IOException {
		assertEquals(regobj.openRegisterPage(), true, "Registration page is not opened ");	
		log.debug("Register Page Opens");
		assertEquals(regobj.fillDetails(email,password,repasswd,FN,LN,Cntry,Ste,ZIP,City,Add1,Add2,phone),true,"home page is not opened ");
		emailid = email;
		registerstate = regobj.homePage();
	}

	
	@BeforeMethod
	public void beforeMethod() {
		regobj=PageFactory.initElements(driver, AvactisRegisterPO.class);
		log = Logger.getRootLogger();
	}

	@AfterMethod
	public void afterMethod() {
		//driver.quit();
		if (registerstate) {
			assertEquals(regobj.clearRegUser(data.getAdminuname(),data.getAdminpwd(), emailid),true,"Not Cleared");
			emailid = "";
			registerstate = false;
		}
	}
}
