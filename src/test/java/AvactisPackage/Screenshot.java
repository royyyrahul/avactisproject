package AvactisPackage;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class Screenshot {

	WebDriver driver;
	private static int fileno=1;
	DateFormat dateFormat = new SimpleDateFormat("dd.MM");
	Date date = new Date();
	String d=new String(dateFormat.format(date));
		public void TakeScreenshot(WebDriver driver) throws IOException {
			this.driver=driver;
			TakesScreenshot ts=(TakesScreenshot)driver;
		File src=ts.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(src,new File ("D:\\New_Workspace\\AvactisProject\\test-output\\Screenshots\\Assert'"+fileno+"''"+d+"'.png"));
			fileno++;

		}
}
