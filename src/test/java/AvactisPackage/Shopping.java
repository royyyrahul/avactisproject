package AvactisPackage;

import static org.testng.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import AvactisPO.AvactisShoppingPO;
import AvactisPO.ConfigData;
import AvactisPO.DataProv;


public class Shopping {
	WebDriver driver;
	AvactisShoppingPO shpobj; 
	ConfigData data=new ConfigData();
	Logger log ;
	@Test(dataProvider = "ProductsAvactis",dataProviderClass=DataProv.class)
	public void AvactisShoppingTesta(String Sno, String Uname, String Pwd, String Category, String Product, String item)
			throws InterruptedException {
        if (Sno.equals("1"))
		{
          shpobj.login(Uname, Pwd);
		}
		assertEquals(shpobj.addProduct(Sno, Category, Product, item), true, "Item is not added to cart");
	}

	@Test
	public void AvactisShoppingTestb() throws InterruptedException{
     assertEquals(shpobj.checkout(), true,"Checkout not done");
	}
	@Test
	public void AvactisShoppingTestc() throws InterruptedException {
		
    assertEquals(shpobj.checkOrders(data.getAdminuname(),data.getAdminpwd()), true,"Orders are not there");
		
	}


	@BeforeMethod
	public void beforeMethod() {
		// System.setProperty("webdriver.chrome.driver",
		// "src/test/resources/chromedriver.exe");

	}

	@AfterMethod
	public void afterMethod() {

	}

	@BeforeClass
	public void beforeClass() {
		shpobj=PageFactory.initElements(driver, AvactisShoppingPO.class);
		log=Logger.getRootLogger();
	}

	@AfterClass
	public void afterClass() {

	}

}
